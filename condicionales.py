numero = 6
#if
if numero == 5:
    print('El numero es 5')
#if and or
if numero >= 5 and numero < 10:
    print('El numero esta entre 5 y 10')
#if-else
if numero == 5:
    print('El numero es 5')
else:
    print('El numero NO es 5')
#if-else-if
if numero == 1:
    print('El numero es 1')
elif numero == 2:
    print('El numero es 2')
elif numero == 3:
    print('El numero es 3')
else: #opcional
    print('El numero no es 1, 2, o 3')


#operador ternario
#valor_si_verdadero if condicion else valor_falso

valor = 10
mensaje = "es 10" if valor == 10 else "no es 10"
print(mensaje)