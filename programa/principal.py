from usu import (
    menu,
    solicitar_valores,
    mostrar_resultado,
    mostrar_opcion_incorrecta,
    mostrar_mensaje_salir,
    SALIR, SUMAR, RESTAR, MULTIPLICAR, DIVIDIR)


from calculadora import (
    sumar,
    restar,
    multiplicar,
    dividir)

# EJECUTAR PROGRAM mientras no se salga
opcion = 0

while opcion != SALIR:
    # pide la opcion al usuario
    opcion = menu()

    # verifica si la opcion digitada es correcta
    if opcion in [SUMAR, RESTAR, MULTIPLICAR, DIVIDIR]:
        # pide los valores al usurio
        x, y = solicitar_valores()
        # verifica la opcion del usuario
        if opcion == SUMAR:
            # suma los valores
            resultado = sumar(x, y)
        elif opcion == RESTAR:
            # resta los valores
            resultado = restar(x, y)
        elif opcion == MULTIPLICAR:
            # multiplicar los valores
            resultado = multiplicar(x, y)
        elif opcion == DIVIDIR:
            # rdividir los valores
            resultado = dividir(x, y)

        # muestra el resultado
        mostrar_resultado(resultado)

    elif opcion == SALIR:
        mostrar_mensaje_salir()
    else:
        mostrar_opcion_incorrecta()
