# constantes
SUMAR = 1
RESTAR = 2
MULTIPLICAR = 3
DIVIDIR = 4
SALIR = 5


def menu():
    '''
    Funcion que muestr el menu y retorna
    la

    '''
    print('\nCALCULADORA')
    print('1. Sumar')
    print('2. Restar')
    print('3. Multiplicar')
    print('4. Dividir')
    print('5. Salir')
    opcion = input('Digite su opcion... ')
    return int(opcion)


def solicitar_valores():
    num1 = input('Digite el numero 1...')
    num2 = input('Digite el numero 2...')
    return int(num1), int(num2)


def mostrar_resultado(valor):
    print(f'El resultado es: {valor}')


def mostrar_opcion_incorrecta():
    print(f'La opcion digitada es incorrecta!!!')


def mostrar_mensaje_salir():
    print(f'Gracias por usar el programa!!!')
