#inicualizacion de variables tr
cadena_1 = 'Hello world'
cadena_2 = "HELLO WORLD"
#imprimir por pantalla
print(cadena_1)
print(cadena_2)

#I can´t program
#mensaje_1 = 'I can't program'
mensaje_1 = 'I can\'t program2'
# con \ se puede escribir con las comillas sencillas 
print(mensaje_1)

mensaje_2 = 'Hi\n love'
# con \n para bajar de renglon
print(mensaje_2)

mensaje_3 = r'c:documentos\nominal\datos.dox'
# con raw, cadena crud tan cual esta para que no interprete nada
print(mensaje_3)
