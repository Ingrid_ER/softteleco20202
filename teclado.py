#Solicitar la edad a el usuario
edad = input('Digite su edad...') #variable siempre sera de tipo string
print(edad)
#obtiene el tipo de dato de la variable
tipo_variable = type(edad)
#manera edecuado para nombres compuestas
#no hay mayusculas en las variables
print(tipo_variable)
#convertir el tipo de dato
edad_entero = int(edad)
print(edad_entero, type(edad_entero))
edad_flotante = float(edad)
print(edad_flotante, type(edad_flotante))
