#inicializacion
x = 0
#ciclo
while x < 5:
    #intruccion que se deaea repetir
    print(f'x = {x}')
    #instruccion de rompimiento
    x = x + 1

print("=" * 20)

# ciclo entre 0 y 3
for numero in range(4):
    print(numero)

print("=" * 20)

# ciclo entre 4 y 9
for numero in range(4, 10):
    print(numero, end=' ')

print("=" * 20)

# ciclo entre 4 y 20 en 3 en 3
for numero in range(4, 21, 3):
    print(numero, end=' ')

#recorrer una lista
equipos = ['nacional', 'millonarios', 'america','junior']
#recorrido con while
i = 0
while i < len(equipos):
    print(equipos[i])
    i = i + 1

print("=" * 20)
#recorrido con for
for equipos in equipos:
    print(equipos)

print("=" * 20)
#matriz recorrer
tablero = [
    ['X', 'O', '-'],
    ['O', 'X', 'O'],
    ['X', 'O', '-'],
]

#fila = 0
#while fila < len(tablero):
#    col = 0
#    while col < len(tablero[fila]):
#        print(tablero[fila][col], end='\t')
#        col = col + 1
#    print('\n')
#    fila = fila + 1
#imprimir tablero con while
fila = 0
print('-' * (len(tablero)*4+1))
while fila < len(tablero):
    col = 0
    while col < len(tablero[fila]):
        print(f'|{tablero[fila][col]}', end='')
        col = col + 1
    print('|')
    print('-' * (len(tablero)*4+1))
    fila = fila + 1


print("*" * 20)
print('-' * (len(tablero)*4+1))
#imprimir tablero con for
for fila in tablero:
    for elemento in fila:
        print(f'|{elemento} ', end='')
    print('|')
    print('-' * (len(tablero)*4+1))