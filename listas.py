# las listas son dinamicas / se pueden modificar
# Declaracion de lista
lista = []
numeros = [1, 2, 3, 4, 5]
print(numeros)
print(f'La longitud es {len(numeros)}')
print(f'El resultado es {2*8}')

print(numeros[0])
print(numeros[-1])
print(numeros[2:])

variada = [1, 3.4, 'cadena', 4, True]
print(variada)
print(type(variada[2]))  # tipo de dato
# concatenando listas
primeros = [1, 2, 3]
ultimos = [4, 5, 6]
todos = primeros + ultimos
print(todos)

todos[0] = 10
print(todos)

#agregar elemntos 
todos.append(100)
print(todos)
#insertar elementos
todos.insert(1, 0)
print(todos)
#cambiar
todos[1:3] =[20, 30]
print(todos)
#elimina una parte de la lista
#se debe seleeccionar uno antes MIRAR BIEN
todos[4:6] = []
print(todos)