from clases import Curso, Persona

p1 = Persona('Hugo', 20)
p1 = Persona('Paco', 21)
p1 = Persona('Luis', 19)

c1 = Curso(100, 'Programacion')
#agregar alementos a un atributo de tipo lista
c1.estudiantes.append(p1)
c1.estudiantes.append(p2)
c1.estudiantes.append(p3)
print(f'El curso {c1.nombre} tiene {len(c1.estudiantes)} estudiantes')

for estudiantes in c1.estudiantes:
    print(estudiantes)