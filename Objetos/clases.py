class Persona:
    nombre = ''
    edad = 0
    

    #metodo de negocio
    def es_adulto(self):
        return self.edad >= 18
        # if self.edad >= 18:
        #     return True
        # else:
        #     return False

# instanciacion p es un objeto basado en
# una clase
# p1 = Persona()
# # acceso a los atributos
# p1.nombre = 'Caralina'
# p1.edad = 18
# # lectura del valor de atributos
# print(p1.nombre)
# print(p1.edad)
# print(f'{p1.nombre} es adulto?, {p1.es_adulto()}')

# print('*'*20)

class Empleado(Persona):
    salario = 0

    def __init__(self, salario=0):
        self.salario = salario

    def __str__(self):
        return f'nombre={self.nombre}, salario={self.salario}'


class Curso:

    '''
    Clase que maneja los datos de un curso
    '''
    codigo = 0
    nombre = ''
    # constructores

    def __init__(self, codigo=0, nombre='Vacio'):
        '''
        Cosntructor del curso
        '''
        self.codigo = codigo
        self.nombre = nombre

    # constructor por defecto
    def __str__(self):
        return f'codigo={self.codigo}, nombre={self.nombre}'


# c1 = Curso(100, 'Programacion')
# print(c1.codigo)
# print(c1.nombre)
# print(c1)

# c2 = Curso(100, 'Programacion')
# c2.codigo = 200
# c2.nombre = 'Base de datos'
# print(c2)

# c3 = Curso(nombre='Circuito', codigo=30)
# print(c3)

# c4 = Curso(nombre='Redes')
# print(c4)

# c5 = Curso(codigo=500)
# print(c5)

# c6 = Curso()
# print(c6)
