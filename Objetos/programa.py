from clases import Curso, Persona, Empleado


p1 = Persona()
# acceso a los atributos
p1.nombre = 'Caralina'
p1.edad = 18
# lectura del valor de atributos
print(p1.nombre)
print(p1.edad)
print(f'{p1.nombre} es adulto?, {p1.es_adulto()}')

print('*'*20)


c1 = Curso(100, 'Programacion')
print(c1.codigo)
print(c1.nombre)
print(c1)

c2 = Curso(100, 'Programacion')
c2.codigo = 200
c2.nombre = 'Base de datos'
print(c2)

c3 = Curso(nombre='Circuito', codigo=30)
print(c3)

c4 = Curso(nombre='Redes')
print(c4)

c5 = Curso(codigo=500)
print(c5)

c6 = Curso()
print(c6)

print('='*20)

e1 = Empleado(salario=2000)
e1.nombre ='Luis Felipe'
e1.edad = 10
print(e1)

if e1.es_adulto():
    print('Es adulto')
else:
    print('Es bebe')
