#accede a cadena caracter
cadena = "universidad"
print(cadena[0])#primer caracter
print(cadena[5])
print(cadena[-1])#ultimo caracter
print(cadena[-2])

#cadena[1] ='U' #no se puede cambial un caracter de una posicion 

print(cadena[0:4])
print(cadena[3:7])#sub cadenas
print(cadena[:7])#desde el inicio hasta el 6
print(cadena[4:])#hasta el final

#METODOS DE STRING

longitud = len(cadena)
print(longitud)

inicia = cadena.startswith('uni') #true
print(inicia)
termina = cadena.endswith('uni') #false
print(termina)

esta = 'si' in cadena
print(esta) #true
esta = 'Si' in cadena
print(esta) #false

posicion = cadena.find('ni')
print(posicion)
posicion1 = cadena.find('xxx') #devuelve un -1 porque no esta 
print(posicion1)

nombre = 'Lina'
edad = 19
formato = f'La estudiante {nombre} tiene {edad} años'
print(formato)
#otra manera
formato_feo = 'La estudiante '+ nombre +' tiene '+str(edad)+' años '
print(formato_feo)
#coloca el mayuscula las iniciales de cada palabra
#capitalizacion de una cadena
frase = 'la casa es bonita'
print(frase.title())

cantidad = frase.count('a')
print(f'La cantidad es {cantidad}')

resultado = frase.replace('a','A')
print(resultado)
#mayusculas y minuscilas
print(frase.upper())
print(frase.lower())

#analisis
clave = 'abc123'
print(clave.isalnum())
print(clave.isalpha())
print(clave.isdecimal()) 
print(clave.islower()) #si todas son mayusculas
print(clave.isupper()) #si todas son minusculas

