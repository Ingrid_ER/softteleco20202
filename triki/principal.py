from ui import (
    # matriz_inicial,
    informacion,
    pos_1,
    pos_2,
    pos_3,
    pos_4,
    pos_5,
    pos_6,
    pos_7,
    pos_8,
    pos_9,
    salir,
    solicitar_valor1,
    solicitar_valor2,
    mensaje_rep,
    mensaje_opcion_incorrecta,
    mensaje_salida,
    mensaje_ganador1,
    mensaje_ganador2,
    mensaje_fin
)

from juego import(
    CAMBIO_JUG1,
    CAMBIO_JUG2,
    valida_ganador1,
    valida_ganador2
)

opcion = 10
jugar = 10
jugador1 = 1
jugador2 = 2
suma = 0

while opcion != salir:
    opcion = informacion()
    if opcion == salir:
        mensaje_salida()
    elif opcion == 1:
        while jugar != salir:
            # turno del primer jugador
            while jugador1 == 1:
                x = solicitar_valor1()
                ver = valida_ganador1(x)
                if x in [pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7, pos_8, pos_9]:
                    revisar = CAMBIO_JUG1(x)
                    if revisar == 0:
                        mensaje_rep()
                    else:
                        if ver == 3:
                            #por si gana jugador 1
                            mensaje_ganador1()
                            mensaje_salida()
                            jugar = salir
                            opcion = salir
                            jugador1 = 0
                        elif ver == 0:
                            #si se acaba el juego
                            mensaje_fin()
                            mensaje_salida()
                            jugar = salir
                            opcion = salir
                            jugador1 = 0
                        else:
                            jugador1 = 0
                            jugador2 = 1
                            # print(ver)
                #si quiere salir del juego
                elif x == salir:
                    mensaje_salida()
                    jugar = salir
                    opcion = salir
                    jugador1 = 0
                #opcion incorrecta
                else:
                    mensaje_opcion_incorrecta()

            # turno del segundo jugador
            while jugador2 == 1:
                y = solicitar_valor2()
                ver1 = valida_ganador2(y)
                if y in [pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7, pos_8, pos_9]:
                    revisar2 = CAMBIO_JUG2(y)
                    if revisar2 == 0:
                        mensaje_rep()
                    else:
                        if ver1 == 3:
                            #si el ganador es jugador 2
                            mensaje_ganador2()
                            mensaje_salida()
                            jugar = salir
                            opcion = salir
                            jugador2 = 0
                        else:
                            #para seguir jugando
                            jugador2 = 0
                            jugador1 = 1

                elif y == salir:
                    #si se quiere salir del juego
                    mensaje_salida()
                    jugar = salir
                    opcion = salir
                    jugador2 = 0
                else:
                    mensaje_opcion_incorrecta()
    else:
        mensaje_opcion_incorrecta()
