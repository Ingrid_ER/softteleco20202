
pos_1 = 1
pos_2 = 2
pos_3 = 3
pos_4 = 4
pos_5 = 5
pos_6 = 6
pos_7 = 7
pos_8 = 8
pos_9 = 9
salir = 0



def informacion():
    print('\n*****BIENVENIDO A TRIKI EL JUEGO******')
    print('\nPor favor tenga en cuenta que las posiciones de la matriz')
    print('son las siguientes: ')
    tablero = [
        ['1', '2', '3'],
        ['4', '5', '6'],
        ['7', '8', '9'],
    ]
    fila = 0
    print('-' * (len(tablero) + 4))
    while fila < len(tablero):
        col = 0
        while col < len(tablero[fila]):
            print(f'|{tablero[fila][col]}', end='')
            col = col + 1
        print('|')
        print('-' * (len(tablero) + 4))
        fila = fila + 1

    #matriz_inicial()
    respuesta = input('******PREPARADOS??***** \nDigite 1, para continuar \nDigite 0, para salir\n')
    return int(respuesta)

# def matriz_inicial():
#     tablero = [
#         [' ', ' ', ' '],
#         [' ', ' ', ' '],
#         [' ', ' ', ' '],
#     ]

#     fila = 0
#     print('-' * (len(tablero) + 4))
#     while fila < len(tablero):
#         col = 0
#         while col < len(tablero[fila]):
#             print(f'|{tablero[fila][col]}', end='')
#             col = col + 1
#         print('|')
#         print('-' * (len(tablero) + 4))
#         fila = fila + 1
    

def solicitar_valor1():
    jug1 = input('Jugador X, digite la posicion que desea: ')
    return int(jug1)

def solicitar_valor2():
    jug2 = input('Jugador O, digite la posicion que desea: ')
    return int(jug2)

def mensaje_rep():
    print('La posicion ya se uso, seleccione OTRA.')

def mensaje_opcion_incorrecta():
    print('La opcion no es valida, verifique el la posicion!!!')

def mensaje_salida():
    print('Gracias por usas TRIKI!!!')

def mensaje_ganador1():
    print('***FELICIDADES!!! GANADOR JUGADOR X***')

def mensaje_ganador2():
    print('***FELICIDADES!!! GANADOR JUGADOR O***')

def mensaje_fin():
    print('***FIN DEL JUEGO!!!***')

