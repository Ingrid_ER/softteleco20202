
matriz_usada = [1, 2, 3, 4, 5, 6, 7, 8, 9]
tablero = [
    [' ', ' ', ' '],
    [' ', ' ', ' '],
    [' ', ' ', ' '],
]
conteo1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
conteo2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
final = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def CAMBIO_JUG1(posicion):
    matriz_original = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    revisar = posicion - 1

    if matriz_usada[revisar] == matriz_original[revisar]:

        if posicion == 1:
            tablero[0][0] = 'X'
        elif posicion == 2:
            tablero[0][1] = 'X'
        elif posicion == 3:
            tablero[0][2] = 'X'
        elif posicion == 4:
            tablero[1][0] = 'X'
        elif posicion == 5:
            tablero[1][1] = 'X'
        elif posicion == 6:
            tablero[1][2] = 'X'
        elif posicion == 7:
            tablero[2][0] = 'X'
        elif posicion == 8:
            tablero[2][1] = 'X'
        elif posicion == 9:
            tablero[2][2] = 'X'

        fila = 0
        print('-' * (len(tablero) + 4))
        while fila < len(tablero):
            col = 0
            while col < len(tablero[fila]):
                print(f'|{tablero[fila][col]}', end='')
                col = col + 1
            print('|')
            print('-' * (len(tablero) + 4))
            fila = fila + 1

        matriz_usada[posicion - 1] = 0
        #print(matriz_usada)

    else:
        devuelva = 0
        return devuelva


def CAMBIO_JUG2(posicion):
    matriz_original = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    revisar = posicion - 1

    if matriz_usada[revisar] == matriz_original[revisar]:

        if posicion == 1:
            tablero[0][0] = 'O'
        elif posicion == 2:
            tablero[0][1] = 'O'
        elif posicion == 3:
            tablero[0][2] = 'O'
        elif posicion == 4:
            tablero[1][0] = 'O'
        elif posicion == 5:
            tablero[1][1] = 'O'
        elif posicion == 6:
            tablero[1][2] = 'O'
        elif posicion == 7:
            tablero[2][0] = 'O'
        elif posicion == 8:
            tablero[2][1] = 'O'
        elif posicion == 9:
            tablero[2][2] = 'O'

        fila = 0
        print('-' * (len(tablero) + 4))
        while fila < len(tablero):
            col = 0
            while col < len(tablero[fila]):
                print(f'|{tablero[fila][col]}', end='')
                col = col + 1
            print('|')
            print('-' * (len(tablero) + 4))
            fila = fila + 1

        matriz_usada[posicion - 1] = 0
        #print(matriz_usada)

    else:
        devuelva = 0
        return devuelva


def valida_ganador1(jugador1):
    valida = jugador1 - 1
    if jugador1 == 1:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 2:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 3:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 4:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 5:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 6:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 7:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 8:
        conteo1[valida] = 0
        final[valida] = 0
    elif jugador1 == 9:
        conteo1[valida] = 0
        final[valida] = 0

    if conteo1[0] == 0 and conteo1[1] == 0 and conteo1[2] == 0:
        gana = 3
        return gana

    if conteo1[3] == 0 and conteo1[4] == 0 and conteo1[5] == 0:
        gana = 3
        return gana

    if conteo1[6] == 0 and conteo1[7] == 0 and conteo1[8] == 0:
        gana = 3
        return gana

    if conteo1[0] == 0 and conteo1[3] == 0 and conteo1[6] == 0:
        gana = 3
        return gana

    if conteo1[1] == 0 and conteo1[4] == 0 and conteo1[7] == 0:
        gana = 3
        return gana

    if conteo1[2] == 0 and conteo1[5] == 0 and conteo1[8] == 0:
        gana = 3
        return gana

    if conteo1[0] == 0 and conteo1[4] == 0 and conteo1[8] == 0:
        gana = 3
        return gana

    if conteo1[2] == 0 and conteo1[4] == 0 and conteo1[6] == 0:
        gana = 3
        return gana
    
    if final[0] == 0 and final[1] == 0 and final[2] == 0 and final[3] == 0 and final[4] == 0 and final[5] == 0 and final[6] == 0 and final[7] == 0 and final[8] == 0:
        gana = 0
        return gana


def valida_ganador2(jugador2):
    valida = jugador2 - 1
    if jugador2 == 1:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 2:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 3:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 4:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 5:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 6:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 7:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 8:
        conteo2[valida] = 0
        final[valida] = 0
    elif jugador2 == 9:
        conteo2[valida] = 0
        final[valida] = 0

    if conteo2[0] == 0 and conteo2[1] == 0 and conteo2[2] == 0:
        gana = 3
        return gana

    if conteo2[3] == 0 and conteo2[4] == 0 and conteo2[5] == 0:
        gana = 3
        return gana

    if conteo2[6] == 0 and conteo2[7] == 0 and conteo2[8] == 0:
        gana = 3
        return gana

    if conteo2[0] == 0 and conteo2[3] == 0 and conteo2[6] == 0:
        gana = 3
        return gana

    if conteo2[1] == 0 and conteo2[4] == 0 and conteo2[7] == 0:
        gana = 3
        return gana

    if conteo2[2] == 0 and conteo2[5] == 0 and conteo2[8] == 0:
        gana = 3
        return gana

    if conteo2[0] == 0 and conteo2[4] == 0 and conteo2[8] == 0:
        gana = 3
        return gana

    if conteo2[2] == 0 and conteo2[4] == 0 and conteo2[6] == 0:
        gana = 3
        return gana
