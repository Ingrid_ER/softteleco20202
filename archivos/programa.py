nombre_archivo = 'banda.txt'
'''
ARCHIVOS PLANOS
'''


# #apertura modod escritura
# # w= elimina el archivo y lo crea de nuevo
# archivo = open('banda.txt', 'w')
# #escritura en el erachivo
# archivo.write('mana\n')
# archivo.write('rock\n')
# #cierre del programa
# archivo.close()

# #apertura de archivo
# # a= agregr informacion
# archivo = open(nombre_archivo, 'a')
# #agrega informacion
# archivo.write('Camila\n')
# archivo.write('ADCD\n')
# archivo.write('Led Zeppilin\n')
# archivo.close()

#apertura del archivo
archivo = open(nombre_archivo, 'r')
#lee todo el contenido y lo guarda en un string
contenido = archivo.read()
#muestra el contenido
print(type(contenido))
print(contenido)
archivo.close()

archivo = open(nombre_archivo, 'r')

linea = archivo.readline()
print(linea)
linea = archivo.readline()
print(linea)
archivo.close()

print('='*20)

#abrir el archivo
archivo = open(nombre_archivo, 'r')
#recorrer el archivo linea por linea
for renglon in archivo:
    print(renglon, end = '')

archivo.close()