import json

# datos = {
#     'nombre': 'guido van rossum',
#     'edad': 60
# }

# archivo = open('datos.json', 'w')
# #guarda el diccionario en un archivo
# json.dump(datos, archivo)
# #cerrar archivo
# archivo.close()
'''
ARCHIVO JASON
'''

# Abrir archivo
archivo = open('datos.json', 'r')
# Carga el contenido del archivo en un diccionario
informacion = json.load(archivo)
# Cierra el archivo
archivo.close()
# Lee una clave del diccionario
print(informacion['edad'])
