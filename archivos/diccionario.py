curso = {}

'''
DICCIONARIOS
'''

curso = {
    'codigo': 100,
    'nombre': 'software',
    'abierto': True,
    'estudiantes': [
        {'id': 1, 'nombre': 'Hugo'},
        {'id': 2, 'nombre': 'Luis'},
        {'id': 3, 'nombre': 'Paco'},
    ],
    'promedios': [1, 2, 3, 4, 5]
}

print(curso)

# curso['codigo'] = 200
# print(curso)
# print('='*20)
# curso['universidad'] = 'USTA'
# print('='*20)
# print(curso)
del curso['abierto']
# print('='*20)
# print(curso)
print('=' * 30)
print(curso['estudiantes'])

if 'abierto' in curso:
    print('SI EXISTE!')
else:
    print('NO EXISTE!')