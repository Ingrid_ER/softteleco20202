#FUNCIONES DE LISTAS
ciudades = ['NY', 'Paris', 'Roma', 'Londres']
print(ciudades)
#longitud
print(f'La longitud es {len(ciudades)}')
#cantidad de ocurrencias
print(ciudades.count('NY'))
#posicion en la lista
print(ciudades.index('Roma'))
ciudades.reverse()
print(ciudades)
#ordenar la lista
ciudades.sort()
print(ciudades)
#elimina un elemento
ciudades.pop(1)
print(ciudades)
ciudades.pop() #elimina el ultimo
print(ciudades)
#elimina un objeto especifico
ciudades.remove('Londres')
print(ciudades)
#verifica si un elemento esta en la lista
print(f'NY es la ciudad es la lista? {"NY" in ciudades} ')
