#definicion de la funcion
def imprimir():
    print('Hello World')

def imprimir_mayusculas(cadena):
    print(cadena.upper())

def sumar(a, b):
    return a + b

n = 10
def imprimir_variable():
    n = 5
    print(n)

#invocar la fucion
imprimir()
imprimir_mayusculas('universidad')

x= sumar(13, 3)
print(x)

imprimir_variable()
print(n)